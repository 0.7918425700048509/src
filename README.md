# src

"What is this place?" you ask.

Mike shakes his head and laughs, %HESITATION% "You wouldn't understand."

# API

- http://localhost:9666/

# Interface

- https://thesource.fm/
- https://discord.gg/Hv7DEW63Tp

# Training data

- [MANIFESTO.txt](/MANIFESTO.txt)
- https://ink.university/
- [https://fate.agency/](https://bafybeigz5tzb7kbxeeb6fd7bka5dk3lxuh4g5hujvszaad4xwyw2yjwhne.ipfs.nftstorage.link/)
- [https://research.gq/](https://research.gq/#/page/getting%20started)

# Dashboard

- http://localhost:6006

# Mirrors

- https://chatbook.dev/
- https://chatbook.org/
- http://Alpha8.888/?channel=backbone.lumbar.6/
- http://Omega8.888.ipns.localhost:8080/?channel=backbone.lumbar.5/
